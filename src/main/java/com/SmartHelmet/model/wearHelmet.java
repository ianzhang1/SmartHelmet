package com.SmartHelmet.model;

import java.util.Date;

/**
 * ���ͷ��
 */
public class wearHelmet {
    public com.SmartHelmet.model.worker getWorker() {
        return worker;
    }

    public void setWorker(com.SmartHelmet.model.worker worker) {
        this.worker = worker;
    }

    worker worker;

    public com.SmartHelmet.model.helmet getHelmet() {
        return helmet;
    }

    public void setHelmet(com.SmartHelmet.model.helmet helmet) {
        this.helmet = helmet;
    }

    helmet helmet;

    private Integer id;

    private Integer helmetId;

    private Integer wokerId;

    private Date created;

    private String remark;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHelmetId() {
        return helmetId;
    }

    public void setHelmetId(Integer helmetId) {
        this.helmetId = helmetId;
    }

    public Integer getWokerId() {
        return wokerId;
    }

    public void setWokerId(Integer wokerId) {
        this.wokerId = wokerId;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark == null ? null : remark.trim();
    }
}