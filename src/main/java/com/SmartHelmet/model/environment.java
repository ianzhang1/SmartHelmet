package com.SmartHelmet.model;

import java.util.Date;

public class environment {
    private Integer id;

    private Integer helmetId;

    private Integer workerId;

    private String co;

    private String nh3;

    private String co2;

    private String nox;

    private String hs;

    private String o2;

    private Date collectTime;

    private Date created;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHelmetId() {
        return helmetId;
    }

    public void setHelmetId(Integer helmetId) {
        this.helmetId = helmetId;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public String getCo() {
        return co;
    }

    public void setCo(String co) {
        this.co = co == null ? null : co.trim();
    }

    public String getNh3() {
        return nh3;
    }

    public void setNh3(String nh3) {
        this.nh3 = nh3 == null ? null : nh3.trim();
    }

    public String getCo2() {
        return co2;
    }

    public void setCo2(String co2) {
        this.co2 = co2 == null ? null : co2.trim();
    }

    public String getNox() {
        return nox;
    }

    public void setNox(String nox) {
        this.nox = nox == null ? null : nox.trim();
    }

    public String getHs() {
        return hs;
    }

    public void setHs(String hs) {
        this.hs = hs == null ? null : hs.trim();
    }

    public String getO2() {
        return o2;
    }

    public void setO2(String o2) {
        this.o2 = o2 == null ? null : o2.trim();
    }

    public Date getCollectTime() {
        return collectTime;
    }

    public void setCollectTime(Date collectTime) {
        this.collectTime = collectTime;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}