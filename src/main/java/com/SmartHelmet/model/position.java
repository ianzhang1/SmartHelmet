package com.SmartHelmet.model;

import java.util.Date;

/**
 * ��λ��Ϣ
 */
public class position {
    private Integer id;

    private Integer helmetId;

    private Integer workerId;

    private String x;

    private String y;

    private String z;

    private Date created;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getHelmetId() {
        return helmetId;
    }

    public void setHelmetId(Integer helmetId) {
        this.helmetId = helmetId;
    }

    public Integer getWorkerId() {
        return workerId;
    }

    public void setWorkerId(Integer workerId) {
        this.workerId = workerId;
    }

    public String getX() {
        return x;
    }

    public void setX(String x) {
        this.x = x == null ? null : x.trim();
    }

    public String getY() {
        return y;
    }

    public void setY(String y) {
        this.y = y == null ? null : y.trim();
    }

    public String getZ() {
        return z;
    }

    public void setZ(String z) {
        this.z = z == null ? null : z.trim();
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}