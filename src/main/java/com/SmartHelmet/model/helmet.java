package com.SmartHelmet.model;

import java.util.Date;

/**
 * 头盔实体
 */
public class helmet {
    private Integer id;

    private String code;

    private String residualElectricity;

    private String workingTime;

    private Date created;

    private Integer status;

    private Integer byUserId;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code == null ? null : code.trim();
    }

    public String getResidualElectricity() {
        return residualElectricity;
    }

    public void setResidualElectricity(String residualElectricity) {
        this.residualElectricity = residualElectricity == null ? null : residualElectricity.trim();
    }

    public String getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(String workingTime) {
        this.workingTime = workingTime == null ? null : workingTime.trim();
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getByUserId() {
        return byUserId;
    }

    public void setByUserId(Integer byUserId) {
        this.byUserId = byUserId;
    }
}