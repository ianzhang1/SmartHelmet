package com.SmartHelmet.ViewModel;

import com.SmartHelmet.model.position;

import java.util.List;

/**
 * 场景画布,包括尺寸和坐标点集合
 */
public class SceneCanvas {


    public int getSceneWidth() {
        return sceneWidth;
    }

    public void setSceneWidth(int sceneWidth) {
        this.sceneWidth = sceneWidth;
    }

    private int sceneWidth;

    public int getSceneLength() {
        return sceneLength;
    }

    public void setSceneLength(int sceneLength) {
        this.sceneLength = sceneLength;
    }

    private int sceneLength;

    public List<position> getPositions() {
        return positions;
    }

    public void setPositions(List<position> positions) {
        this.positions = positions;
    }

    private List<position> positions;
}
