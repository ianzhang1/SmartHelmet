package com.SmartHelmet.ViewModel;

/**
 * Created by ianzhang on 16/11/28.
 */
public class LoginModel {
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    private String email;

    private String password;
}
