package com.SmartHelmet.ViewModel;

/**
 * 返回结果封装
 */
public class ResponseResult {
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    private String code;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    private String message;

    public Object getData() {
        return Data;
    }

    public void setData(Object data) {
        Data = data;
    }

    private Object Data;
}
