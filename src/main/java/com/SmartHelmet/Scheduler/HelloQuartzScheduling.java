package com.SmartHelmet.Scheduler;
import org.quartz.*;
import org.quartz.impl.StdSchedulerFactory;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/6.
 */
public class HelloQuartzScheduling {
    SchedulerFactory schedulerFactory = new StdSchedulerFactory();
    Scheduler scheduler = schedulerFactory.getScheduler();

    JobDetail jobDetail = new JobDetail("helloQuartzJob",
            Scheduler.DEFAULT_GROUP, HelloQuartzJob.class);

    SimpleTrigger simpleTrigger = new SimpleTrigger("simpleTrigger",Scheduler.DEFAULT_GROUP);

    simpleTrigger.setStartTime(new Date(System.currentTimeMillis()));
    simpleTrigger.setRepeatInterval(5000);
    simpleTrigger.setRepeatCount(10);

    scheduler.scheduleJob(jobDetail, simpleTrigger);

    scheduler.start();
}
