package com.SmartHelmet.service;

import com.SmartHelmet.model.video;

import java.util.List;

/**
 * Created by ianzhang on 16/12/2.
 */
public interface VideoService {

    public List<video> getAllVideos();

    public boolean addVideo(video model);

    public boolean delVideo(int videoId);

    public video getVideoById(int videoId);

    public List<video> getVideos(int wokeryId,String date,int page,int pageSize);

    public boolean updateVideo(video model);
}
