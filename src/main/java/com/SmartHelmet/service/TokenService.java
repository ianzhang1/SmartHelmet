package com.SmartHelmet.service;

import com.SmartHelmet.model.token;

/**
 * Created by Administrator on 2016/12/6.
 */
public interface TokenService {

    public String addToken(String verifyCode);

    public token getLastTokenModel(String token);
}
