package com.SmartHelmet.service;

import com.SmartHelmet.model.Email;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Created by Administrator on 2016/12/5.
 */
public interface MailService {
    public void sendMail(Email email)  throws MessagingException, IOException;

    public void sendMailByAsynchronousMode(final Email email)  throws MessagingException, IOException;

    public void sendMailBySynchronizationMode(Email email) throws MessagingException, IOException;
}
