package com.SmartHelmet.service;

import com.SmartHelmet.model.vitalSigns;

import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */
public interface VitalSignsService {
    public boolean add(vitalSigns model);

    public vitalSigns getLastInfoByWorker(int workeryId);

    public List<vitalSigns> get_heart_rateByDate(int workerId,String date);

    public List<vitalSigns> get_temperature_ByDate(int workerId,String date);

    public List<vitalSigns> get_blood_pressure_ByDate(int workerId,String date);
}
