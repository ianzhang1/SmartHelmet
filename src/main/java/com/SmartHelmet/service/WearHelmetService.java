package com.SmartHelmet.service;

import com.SmartHelmet.model.wearHelmet;
import com.SmartHelmet.model.worker;

import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */
public interface WearHelmetService {
    /**
     * 佩戴头盔
     * @param workerId
     * @param helmetId
     * @return
     */
    public boolean wear(int workerId,int helmetId);

    /**
     * 根据头盔id获取佩戴的员工信息
     * @param helmetId
     * @return
     */
    public worker getWorkerByHelmet(int helmetId);

    public wearHelmet getByWorker(int workerId);

    public worker getId(int id);

    public List<wearHelmet> getAllList();

    public boolean deleteWear(int id);
}
