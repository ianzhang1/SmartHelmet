package com.SmartHelmet.service;

import com.SmartHelmet.model.environment;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */
public interface EnvironmentService {
    public boolean add(environment model);

    /**
     * 获取指定工人最后一次采集到的环境信息
     * @param workerId
     * @return
     */
    public environment getLastByWorker(int workerId);

    public List<environment> getListByDate(int workerId,String date);

    public List<environment> get_CO_ByDate(int workerId,String date);

    public List<environment> get_nh3_ByDate(int workerId,String date);
    public List<environment> get_co2_ByDate(int workerId,String date);
    public List<environment> get_nox_ByDate(int workerId,String date);
    public List<environment> get_hs_ByDate(int workerId,String date);
    public List<environment> get_o2_ByDate(int workerId,String date);
}
