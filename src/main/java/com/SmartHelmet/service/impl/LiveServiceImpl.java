package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.helmetMapper;
import com.SmartHelmet.model.live;
import com.SmartHelmet.service.LiveService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/12/1.
 */
@Service
public class LiveServiceImpl implements LiveService{
    @Resource
    com.SmartHelmet.dao.liveMapper liveMapper;

    @Override
    public List<live> getAllLive() {
        return liveMapper.selectAll();
    }

    @Override
    public List<live> getLivesByStatus(int status) {
        return liveMapper.selectByStatus(status);
    }

    @Override
    public boolean addNew(live model) {
        return liveMapper.insert(model)>0?true:false;
    }

    @Override
    public boolean updateLiveStatus(int liveId, int status) {
        HashMap<String,Integer> map=new HashMap<String,Integer>();
        map.put("id",liveId);
        map.put("status",status);
        //
        return liveMapper.updateStatus(map)>0?true:false;
    }

    @Override
    public boolean updateLive(live model) {
        return liveMapper.updateByPrimaryKey(model)>0?true:false;
    }

    @Override
    public live getLiveingByWorker(int workerId) {
        return liveMapper.selectLiveingByWorker(workerId);
    }

    @Override
    public boolean deleteLive(int liveId) {
        return liveMapper.deleteByPrimaryKey(liveId)>0?true:false;
    }

    @Override
    public live getLiveById(int id) {
        return liveMapper.selectByPrimaryKey(id);
    }
}
