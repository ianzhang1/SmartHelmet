package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.environmentMapper;
import com.SmartHelmet.model.environment;
import com.SmartHelmet.service.EnvironmentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Administrator on 2016/12/2.
 */
@Service
class EnvironmentServiceImpl implements EnvironmentService {
    @Resource
    environmentMapper environmentMapper;

    @Override
    public boolean add(environment model) {
        return environmentMapper.insert(model)>0?true:false;
    }

    @Override
    public environment getLastByWorker(int workerId) {
        return environmentMapper.getLastByWorker(workerId);
    }

    @Override
    public List<environment> getListByDate(int workerId,String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        return environmentMapper.getListByDate(map);
    }

    @Override
    public List<environment> get_CO_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        List<environment> result=environmentMapper.get_CO_ByDate(map);
        fillDataForNullDate(date, result, "co");

        return result;
    }

    /**
     * 循环处理0点至23:30，为空数据问题
     * @param date
     * @param result
     * @param gasType
     */
    private void fillDataForNullDate(String date, List<environment> result,String gasType) {
        Date d=new Date();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM-dd");

        try {
            boolean hasInhour=false;
            String startDateStr=df.format(df.parse(date+" 00:00:00").getTime());
            //从0点到24点，每半小时为步长进行循环
            while (df1.parse(date).compareTo(df1.parse(startDateStr))==0)
            {
                for (environment item : result)
                {
                    if(item.getCreated().compareTo(df.parse(startDateStr))==0)
                    {
                        hasInhour=true;
                        break;
                    }
                }

                if(!hasInhour)
                {
                    environment nullEnvironment=new environment();
                    if(gasType.equalsIgnoreCase("co"))
                        nullEnvironment.setCo("0");

                    if(gasType.equalsIgnoreCase("nh3"))
                        nullEnvironment.setNh3("0");

                    if(gasType.equalsIgnoreCase("co2"))
                        nullEnvironment.setCo2("0");

                    if(gasType.equalsIgnoreCase("nox"))
                        nullEnvironment.setNox("0");

                    if(gasType.equalsIgnoreCase("hs"))
                        nullEnvironment.setHs("0");

                    if(gasType.equalsIgnoreCase("o2"))
                        nullEnvironment.setO2("0");

                    nullEnvironment.setCreated(df.parse(startDateStr));
                    result.add(nullEnvironment);
                }
                startDateStr=df.format(df.parse(startDateStr).getTime() + (30 * 60 * 1000));
                hasInhour=false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        result.sort(new Comparator<environment>() {
            @Override
            public int compare(environment h1, environment h2) {
                return h1.getCreated().compareTo(h2.getCreated());
            }
        });
    }

    @Override
    public List<environment> get_nh3_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        List<environment> result= environmentMapper.get_nh3_ByDate(map);
        fillDataForNullDate(date, result, "nh3");

        return result;
    }

    @Override
    public List<environment> get_co2_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        List<environment> result= environmentMapper.get_co2_ByDate(map);
        fillDataForNullDate(date, result, "co2");

        return result;
    }

    @Override
    public List<environment> get_nox_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        List<environment> result= environmentMapper.get_nox_ByDate(map);
        fillDataForNullDate(date, result, "nox");

        return result;
    }

    @Override
    public List<environment> get_hs_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        List<environment> result= environmentMapper.get_hs_ByDate(map);
        fillDataForNullDate(date, result, "hs");

        return result;
    }

    @Override
    public List<environment> get_o2_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        List<environment> result= environmentMapper.get_o2_ByDate(map);
        fillDataForNullDate(date, result, "o2");

        return result;
    }
}
