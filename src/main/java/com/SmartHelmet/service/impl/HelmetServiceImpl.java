package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.helmetMapper;
import com.SmartHelmet.model.helmet;
import com.SmartHelmet.service.HelmetService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by ianzhang on 16/11/29.
 */
@Service
public class HelmetServiceImpl implements HelmetService {
    @Resource
    helmetMapper helmetMapper;

    @Override
    public boolean updateHelmet(helmet model) {
        return helmetMapper.updateByPrimaryKey(model)>0?true:false;
    }

    @Override
    public helmet getByCode(String code) {
        return helmetMapper.selectByCode(code);
    }

    @Override
    public helmet getById(int id) {
        return helmetMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<helmet> getAll() {
        return helmetMapper.selectAll();
    }

    @Override
    public boolean addNew(helmet model)
    {
        return helmetMapper.insert(model)>0?true:false;
    }

    @Override
    public boolean deleteHelmet(int id) {
        return helmetMapper.deleteByPrimaryKey(id)>0?true:false;
    }
}
