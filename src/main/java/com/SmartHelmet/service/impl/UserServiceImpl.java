package com.SmartHelmet.service.impl;

import com.SmartHelmet.ViewModel.LoginModel;
import com.SmartHelmet.model.master;
import com.SmartHelmet.service.UserService;
import com.SmartHelmet.utils.CryptoUtil;
import com.SmartHelmet.utils.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by Administrator on 2016/11/28.
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private com.SmartHelmet.dao.masterMapper masterMapper;

    @Override
    public master getUser(int id) {
        return masterMapper.selectByPrimaryKey(Integer.valueOf(id));
    }

    @Override
    public master login(LoginModel model) {
        model.setPassword(CryptoUtil.MD5(model.getPassword()));
        master master= masterMapper.selectByUserNameAndPWD(model);
        if(master!=null)
        {
            master.setToken(StringUtil.GetUUID());
            this.UpdateMaster(master);
        }
        return master;
    }

    public boolean UpdateMaster(master model)
    {
        return masterMapper.updateByPrimaryKey(model)>0?true:false;
    }

    @Override
    public master verifyToken(String token) {
        return masterMapper.selectByToken(token);
    }

    @Override
    public List<master> getAllMaster() {
        return masterMapper.selectAllMaster();
    }

    @Override
    public boolean newMaster(master master) {
        return masterMapper.insert(master)>0?true:false;
    }

    @Override
    public master getMasterByEmail(String email) {
        return masterMapper.selectByEmail(email);
    }

    @Override
    public boolean deleteUser(int userId) {
        return masterMapper.deleteByPrimaryKey(userId)>0?true:false;
    }
}
