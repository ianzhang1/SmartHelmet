package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.tokenMapper;
import com.SmartHelmet.model.token;
import com.SmartHelmet.service.TokenService;
import com.SmartHelmet.utils.StringUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by Administrator on 2016/12/6.
 */
@Service
public class TokenServiceImpl implements TokenService {
    @Resource
    tokenMapper tokenMapper;

    @Override
    public String addToken(String verifyCode) {
        String tokenStr= StringUtil.GetUUID();
        token model=new token();
        model.setCreated(new Date());
        model.setToken(tokenStr);
        model.setVerifyCode(verifyCode);
        //
        if(tokenMapper.insert(model)>0)
            return tokenStr;
        return null;
    }

    @Override
    public token getLastTokenModel(String token) {
        return tokenMapper.getLastTokenModel(token);
    }
}
