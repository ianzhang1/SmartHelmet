package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.videoMapper;
import com.SmartHelmet.model.video;
import com.SmartHelmet.service.VideoService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ianzhang on 16/12/2.
 */
@Service
public class VideoServiceImpl implements VideoService {
    @Resource
    videoMapper videoMapper;

    @Override
    public List<video> getAllVideos() {
        return videoMapper.selectAll();
    }

    @Override
    public boolean addVideo(video model) {
        return videoMapper.insert(model)>0?true:false;
    }

    @Override
    public boolean delVideo(int videoId) {
        return videoMapper.deleteByPrimaryKey(videoId)>0?true:false;
    }

    @Override
    public video getVideoById(int videoId) {
        return videoMapper.selectByPrimaryKey(videoId);
    }

    @Override
    public List<video> getVideos(int workerId, String date,int page,int pageSize) {
        HashMap<String,Object> map=new HashMap<String, Object>();
        map.put("workerId", workerId);
        map.put("date", date);

        int offset=(page-1)*pageSize;
        map.put("offset", offset);
        map.put("pageSize", pageSize);
        //
        return videoMapper.selectByDate(map);
    }

    @Override
    public boolean updateVideo(video model) {
        return videoMapper.updateByPrimaryKey(model)>0?true:false;
    }
}
