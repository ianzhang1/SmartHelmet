package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.wearHelmetMapper;
import com.SmartHelmet.dao.workerMapper;
import com.SmartHelmet.model.wearHelmet;
import com.SmartHelmet.model.worker;
import com.SmartHelmet.service.WearHelmetService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */
@Service
public class WearHelmetServiceImpl implements WearHelmetService {
    @Resource
    wearHelmetMapper wearHelmetMapper;

    @Resource
    workerMapper workerMapper;

    /**
     * 获取指定工位是否配带头盔
     * @param workerId
     * @return
     */
    private boolean hasHelmet(int workerId)
    {
        wearHelmet wearModel= wearHelmetMapper.selectByWorker(workerId);
        if(wearModel!=null)
            return true;
        return false;
    }
    /**
     * 为指定的工人佩戴头盔
     * @param workerId
     * @param helmetId
     * @return
     */
    @Override
    public boolean wear(int workerId, int helmetId) {
        wearHelmet wearModel= wearHelmetMapper.selectByWorker(workerId);

        if (wearModel!=null)
        {
            if(wearHelmetMapper.selectByHelmet(helmetId)==null)
            {
                wearModel.setHelmetId(helmetId);
                wearModel.setCreated(new Date());
                return wearHelmetMapper.updateByPrimaryKey(wearModel)>0?true:false;
            }
            else
            {
                //头盔已绑定给其它工位
                return false;
            }
        }
        else
        {
            if(wearHelmetMapper.selectByHelmet(helmetId)==null)
            {
                wearHelmet model=new wearHelmet();
                model.setCreated(new Date());
                model.setHelmetId(helmetId);
                model.setWokerId(workerId);
                //
                return wearHelmetMapper.insert(model)>0?true:false;
            }
            else
            {
                //头盔已绑定给其它工位
                return false;
            }
        }
    }

    @Override
    public worker getWorkerByHelmet(int helmetId) {
        wearHelmet wearModel=wearHelmetMapper.selectByHelmet(helmetId);
        if(wearModel!=null)
        {
            int workerId=wearModel.getWokerId();
            return workerMapper.selectByPrimaryKey(workerId);
        }
        return null;
    }

    @Override
    public wearHelmet getByWorker(int workerId) {
        return wearHelmetMapper.selectByWorker(workerId);
    }

    @Override
    public worker getId(int id) {
        return workerMapper.selectByPrimaryKey(id);
    }

    @Override
    public List<wearHelmet> getAllList() {
        return wearHelmetMapper.selectAll();
    }

    @Override
    public boolean deleteWear(int id) {
        return wearHelmetMapper.deleteByPrimaryKey(id)>0?true:false;
    }
}
