package com.SmartHelmet.service.impl;

import com.SmartHelmet.model.vitalSigns;
import com.SmartHelmet.service.VitalSignsService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/12/2.
 */
@Service
public class VitalSignsServiceImpl implements VitalSignsService{
    @Resource
    com.SmartHelmet.dao.vitalSignsMapper vitalSignsMapper;

    @Override
    public boolean add(vitalSigns model) {
        return vitalSignsMapper.insert(model)>0?true:false;
    }

    @Override
    public vitalSigns getLastInfoByWorker(int workeryId) {
        return vitalSignsMapper.getLastInfoByWorker(workeryId);
    }

    @Override
    public List<vitalSigns> get_heart_rateByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        //
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        List<vitalSigns> result= vitalSignsMapper.get_heart_rateByDate(map);
        fillDataForNullDate(date,result,"heartrate");
        return result;
    }

    @Override
    public List<vitalSigns> get_temperature_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        //
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        List<vitalSigns> result= vitalSignsMapper.get_temperature_ByDate(map);
        fillDataForNullDate(date,result,"temperature");
        return result;
    }

    @Override
    public List<vitalSigns> get_blood_pressure_ByDate(int workerId, String date) {
        HashMap<String,Object> map=new HashMap<String,Object>();
        //
        map.put("workerId",Integer.valueOf(workerId));
        map.put("date",date);
        //
        List<vitalSigns> result= vitalSignsMapper.get_blood_pressure_ByDate(map);
        fillDataForNullDate(date,result,"bloodpressure");
        return result;
    }

    /**
     * 循环处理0点至23:30，为空数据问题
     * @param date
     * @param result
     * @param dataType
     */
    private void fillDataForNullDate(String date, List<vitalSigns> result,String dataType) {
        Date d=new Date();
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        SimpleDateFormat df1=new SimpleDateFormat("yyyy-MM-dd");

        try {
            boolean hasInhour=false;
            String startDateStr=df.format(df.parse(date+" 00:00:00").getTime());
            //从0点到24点，每半小时为步长进行循环
            while (df1.parse(date).compareTo(df1.parse(startDateStr))==0)
            {
                for (vitalSigns item : result)
                {
                    if(item.getCreated().compareTo(df.parse(startDateStr))==0)
                    {
                        hasInhour=true;
                        break;
                    }
                }

                if(!hasInhour)
                {
                    vitalSigns nullVitalSigns=new vitalSigns();
                    if(dataType.equalsIgnoreCase("heartrate"))
                        nullVitalSigns.setHeartRate("0");

                    if(dataType.equalsIgnoreCase("temperature"))
                        nullVitalSigns.setTemperature("0");

                    if(dataType.equalsIgnoreCase("bloodpressure"))
                        nullVitalSigns.setBloodPressure("0");

                    nullVitalSigns.setCreated(df.parse(startDateStr));
                    result.add(nullVitalSigns);
                }
                startDateStr=df.format(df.parse(startDateStr).getTime() + (30 * 60 * 1000));
                hasInhour=false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        result.sort(new Comparator<vitalSigns>() {
            @Override
            public int compare(vitalSigns h1, vitalSigns h2) {
                return h1.getCreated().compareTo(h2.getCreated());
            }
        });
    }
}
