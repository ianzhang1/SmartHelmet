package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.workerMapper;
import com.SmartHelmet.model.worker;
import com.SmartHelmet.service.WorkerService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Administrator on 2016/11/28.
 */
@Service("workerService")
public class WorkerServiceImpl implements WorkerService {

    @Resource
    private workerMapper workerMapper;

    @Override
    public worker getWorker(int id) {
        return this.workerMapper.selectByPrimaryKey(Integer.valueOf(id));
    }

    @Override
    public List<worker> getAllWorkers() {
        return workerMapper.selectAll();
    }

    @Override
    public List<worker> getWorkers(int offset, int pageSize) {
        HashMap<String,Object> map=new HashMap<String, Object>();
        map.put("offset",offset);
        map.put("pageSize",pageSize);
        //
        return workerMapper.getWorkersByPage(map);
    }

    @Override
    public boolean addNew(worker model) {
        return workerMapper.insert(model)>0?true:false;
    }

    @Override
    public boolean deleteWokery(int id) {
        return workerMapper.deleteByPrimaryKey(id)>0?true:false;
    }

    @Override
    public boolean updateWorker(worker model) {
        return workerMapper.updateByPrimaryKey(model)>0?true:false;
    }
}
