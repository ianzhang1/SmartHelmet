package com.SmartHelmet.service.impl;

import com.SmartHelmet.dao.positionMapper;
import com.SmartHelmet.model.position;
import com.SmartHelmet.service.PositionService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;

/**
 * ������Ϣ
 */
@Service
public class PositionServiceImpl implements PositionService {
    @Resource
    private positionMapper positionMapper;

    @Override
    public boolean addPosition(position model) {
        return positionMapper.insert(model)>0?true:false;
    }

    @Override
    public List<position> getPositions(String date, int workerId) {
        HashMap<String,Object> map=new HashMap<String, Object>();
        map.put("date",date);
        map.put("workerId",workerId);
        //
        return positionMapper.selectByDateAndWorkerId(map);
    }
}
