package com.SmartHelmet.service;

import com.SmartHelmet.model.position;

import java.util.List;

/**
 * Created by Administrator on 2016/12/8.
 */
public interface PositionService {
    public boolean addPosition(position model);

    public List<position> getPositions(String date,int workerId);

}
