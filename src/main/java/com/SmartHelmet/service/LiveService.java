package com.SmartHelmet.service;

import com.SmartHelmet.model.live;

import java.util.List;

/**
 * Created by Administrator on 2016/12/1.
 */
public interface LiveService {
    public List<live> getAllLive();

    /**
     * 根据直播状态获取直播
     * @param status
     * @return
     */
    public List<live> getLivesByStatus(int status);

    public boolean addNew(live model);

    public boolean updateLiveStatus(int liveId,int status);

    public boolean updateLive(live model);

    /**
     * 获取某工位的直播数据
     * @param workerId
     * @return
     */
    public live getLiveingByWorker(int workerId);

    public boolean deleteLive(int liveId);

    public live getLiveById(int id);
}
