package com.SmartHelmet.service;

import com.SmartHelmet.ViewModel.LoginModel;
import com.SmartHelmet.model.master;

import java.util.List;

/**
 * Created by Administrator on 2016/11/28.
 */
public interface UserService {
    public master getUser(int id);

    public master login(LoginModel model);

    public boolean UpdateMaster(master model);

    /**
     * ��֤token�Ƿ����
     * @param token
     * @return
     */
    public master verifyToken(String token);

    public List<master> getAllMaster();

    public boolean newMaster(master master);

    public master getMasterByEmail(String email);

    public boolean deleteUser(int userId);
}
