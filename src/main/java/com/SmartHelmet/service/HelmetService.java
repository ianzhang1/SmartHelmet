package com.SmartHelmet.service;

import com.SmartHelmet.model.helmet;

import java.util.List;

/**
 * Created by ianzhang on 16/11/29.
 */
public interface HelmetService {

    /**
     * 更新头盔信息
     * @param model
     * @return
     */
    public boolean updateHelmet(helmet model);


    /**
     * 根据代码获取头盔信息
     * @param code
     * @return
     */
    public helmet getByCode(String code);

    public helmet getById(int id);

    public List<helmet> getAll();

    public boolean addNew(helmet model);

    public boolean deleteHelmet(int id);
}
