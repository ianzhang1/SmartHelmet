package com.SmartHelmet.service;

import com.SmartHelmet.model.worker;

import java.util.List;

/**
 * Created by Administrator on 2016/11/28.
 */
public interface WorkerService {
    public worker getWorker(int id);

    public List<worker> getAllWorkers();

    public List<worker> getWorkers(int offset, int pageSize);

    public boolean addNew(worker model);

    public boolean deleteWokery(int id);

    public boolean updateWorker(worker model);
}
