package com.SmartHelmet.Web;

import com.SmartHelmet.ViewModel.LoginModel;
import com.SmartHelmet.model.master;
import com.SmartHelmet.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * Created by ianzhang on 16/11/29.
 */

@Controller
@RequestMapping("/")
public class LoginController {
    @Resource
    UserService userService;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "login";
    }

    @RequestMapping(value ="login", method = RequestMethod.POST)
    public String login(HttpSession session,String email,String password) {
        LoginModel loginModel=new LoginModel();
        loginModel.setEmail(email);
        loginModel.setPassword(password);
        //
        master master= userService.login(loginModel);
        if(master!=null && master.getEmail().equalsIgnoreCase("admin@babaali.com"))
        {
            session.setAttribute("email", email);
            return "redirect:home/index";
        }
        else
            return "redirect:/";
    }

    @RequestMapping(value="/logout")
    public String logout(HttpSession session) throws Exception{
        //清除Session
        session.invalidate();
        //
        return "redirect:/";
    }
}
