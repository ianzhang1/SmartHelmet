package com.SmartHelmet.Web;

import com.SmartHelmet.model.live;
import com.SmartHelmet.model.worker;
import com.SmartHelmet.service.HelmetService;
import com.SmartHelmet.service.LiveService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;
import com.SmartHelmet.config.liveStatus;

/**
 * Created by Administrator on 2016/12/1.
 */
@Controller
@RequestMapping("/LiveManager")
public class LiveManagerController {

    private String httpHost="http://114.215.201.40:8080/SmartHelmet";

    @Resource
    LiveService liveService;

    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String List(Model model)
    {
        model.addAttribute("lives",liveService.getAllLive());
        return "LiveManager/list";
    }

    @RequestMapping(value = "new",method = RequestMethod.GET)
    public String newLive()
    {
        return "LiveManager/new";
    }

    @RequestMapping(value = "new",method = RequestMethod.POST)
    public String saveNewLive(@RequestParam(value = "pic_input", required = false) MultipartFile file, HttpServletRequest request)
    {
        String path = request.getSession().getServletContext().getRealPath("/WEB-INF/uploads/live_screenshot");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String live_screenshot_FileName = sdf.format(new Date())+file.getOriginalFilename();
        File targetFile = new File(path, live_screenshot_FileName);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }

        //����
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        live model=new live();
        model.setTitle(request.getParameter("title"));
        model.setPushUrl(request.getParameter("pushUrl"));
        model.setRemark(request.getParameter("remark"));
        model.setHelmentId(Integer.valueOf(request.getParameter("helmentId")));
        model.setHlsPullUrl(request.getParameter("hlsPullUrl"));
        model.setHttpPullUrl(request.getParameter("httpPullUrl"));
        model.setRtmpPullUrl(request.getParameter("rtmpPullUrl"));
        model.setLiveImg(httpHost+"/uploads/live_screenshot/"+live_screenshot_FileName);
        model.setWorkerId(Integer.valueOf(request.getParameter("workerId")));
        model.setStatus(liveStatus.Not_Start.ordinal());
        model.setCreated(new Date());
        liveService.addNew(model);
        //
        return "redirect:/LiveManager/list";
    }

    @RequestMapping(value = "start/{id}/{status}",method = RequestMethod.GET)
    public String start(@PathVariable int id,@PathVariable int status)
    {
        liveService.updateLiveStatus(id,status);
        return "redirect:/LiveManager/list";
    }

    /**
     * ����ֱ��
     * @param id
     * @return
     */
    @RequestMapping(value = "end/{id}/{status}",method = RequestMethod.GET)
    public String end(@PathVariable int id,@PathVariable int status)
    {
        liveService.updateLiveStatus(id,status);
        return "redirect:/LiveManager/list";
    }

    @RequestMapping(value = "del/{id}",method = RequestMethod.GET)
    public String del(@PathVariable int id,@PathVariable int status)
    {
        liveService.deleteLive(id);
        return "redirect:/LiveManager/list";
    }

    @RequestMapping(value = "modify/{id}",method = RequestMethod.GET)
    public String modify(@PathVariable int id,Model model)
    {
        model.addAttribute("live",liveService.getLiveById(id));
        return "LiveManager/modify";
    }

    @RequestMapping(value = "modify",method = RequestMethod.POST)
    public String modify(@RequestParam(value = "pic_input", required = false) MultipartFile file, HttpServletRequest request,live model)
    {
        if(file.getSize()>0)
        {
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/uploads/live_screenshot");
            SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
            String live_screenshot_FileName = sdf.format(new Date())+file.getOriginalFilename();
            File targetFile = new File(path, live_screenshot_FileName);
            if(!targetFile.exists()){
                targetFile.mkdirs();
            }

            //����
            try {
                file.transferTo(targetFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            model.setLiveImg(httpHost+"/uploads/live_screenshot/"+live_screenshot_FileName);
        }
        model.setCreated(new Date());
        liveService.updateLive(model);
        return "redirect:/LiveManager/list";
    }
}
