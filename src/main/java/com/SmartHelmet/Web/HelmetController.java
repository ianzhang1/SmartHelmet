package com.SmartHelmet.Web;

import com.SmartHelmet.model.helmet;
import com.SmartHelmet.model.master;
import com.SmartHelmet.service.HelmetService;
import com.SmartHelmet.service.UserService;
import com.SmartHelmet.utils.CryptoUtil;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import java.util.Date;

/**
 * Created by ianzhang on 16/11/30.
 */
@Controller
@RequestMapping("/HelmetManager")
public class HelmetController {

    @Resource
    HelmetService helmetService;

    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String List(Model model)
    {
        model.addAttribute("helmets",helmetService.getAll());
        return "HelmetManager/list";
    }


    @RequestMapping(value = "new",method = RequestMethod.GET)
    public String newUser()
    {
        return "HelmetManager/new";
    }

    @RequestMapping(value = "new",method = RequestMethod.POST)
    public String saveNewUser(helmet model)
    {
        helmetService.addNew(model);
        //
        return "redirect:/HelmetManager/list";
    }

    @RequestMapping(value = "del/{id}",method = RequestMethod.GET)
    public String del(@PathVariable int id)
    {
        helmetService.deleteHelmet(id);
        return "redirect:/HelmetManager/list";
    }

    @RequestMapping(value = "modify/{id}",method = RequestMethod.GET)
     public String modify(@PathVariable int id,Model model)
    {
        model.addAttribute("helmet",helmetService.getById(id));
        return "HelmetManager/modify";
    }

    @RequestMapping(value = "modify",method = RequestMethod.POST)
    public String save_modify(helmet model)
    {
        model.setCreated(new Date());
        helmetService.updateHelmet(model);
        return "redirect:/HelmetManager/list";
    }
}
