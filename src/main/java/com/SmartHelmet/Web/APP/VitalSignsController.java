package com.SmartHelmet.Web.APP;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.model.environment;
import com.SmartHelmet.model.vitalSigns;
import com.SmartHelmet.service.VitalSignsService;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.SmartHelmet.config.returnCode;
import javax.annotation.Resource;
import java.util.List;

/**
 * 生命体征
 * Created by ianzhang on 16/12/3.
 */
@Controller
@RequestMapping("/app/api/vitalSigns")
public class VitalSignsController {
    @Resource
    VitalSignsService vitalSignsService;

    @ResponseBody
    @RequestMapping(value = "/latest", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getLastByWorker(int workerId) {
        ResponseResult pr=new ResponseResult();
        vitalSigns model=vitalSignsService.getLastInfoByWorker(workerId);
        if(model!=null)
        {
            pr.setCode(java.lang.String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setData(model);
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("没有找到相关生命体征信息");
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/heartrate", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_heart_rateByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<vitalSigns> result=vitalSignsService.get_heart_rateByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
        }
        else
        {
            pr.setMessage("没有找到相关生命体征信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
        }
        return JSON.toJSONString(pr);
    }

    @ResponseBody
    @RequestMapping(value = "/temperature", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_temperature_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<vitalSigns> result=vitalSignsService.get_temperature_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关生命体征信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/bloodpressure", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_blood_pressure_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<vitalSigns> result=vitalSignsService.get_blood_pressure_ByDate(workerId,date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关生命体征信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }
}
