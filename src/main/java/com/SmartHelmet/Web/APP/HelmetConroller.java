package com.SmartHelmet.Web.APP;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.config.returnCode;
import com.SmartHelmet.model.helmet;
import com.SmartHelmet.model.wearHelmet;
import com.SmartHelmet.service.HelmetService;
import com.SmartHelmet.service.WearHelmetService;
import com.SmartHelmet.utils.StringUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;

/**
 * Created by Administrator on 2016/12/9.
 */
@Controller
@RequestMapping("/app/api")
public class HelmetConroller {
    @Resource
    HelmetService helmetService;

    @Resource
    WearHelmetService wearHelmetService;

    @ResponseBody
    @RequestMapping(value = "helmetInfoByCode", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getHelmetInfoByCode(String code)
    {
        ResponseResult pr=new ResponseResult();
        if(StringUtil.isEmpty(code))
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("必要的参数不能为空");
            return JSON.toJSONString(pr);
        }

        helmet model=helmetService.getByCode(code);
        if(model!=null)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setData(model);
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("头盔不存在");
            pr.setData(null);
            return JSON.toJSONString(pr);
        }
    }

    /**
     * 根据工位获取其配戴的头盔信息
     * @param workerId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "helmetInfo", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getHelmetInfoByWorker(String workerId)
    {
        ResponseResult pr=new ResponseResult();
        if(StringUtil.isEmpty(workerId))
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("必要的参数不能为空");
            return JSON.toJSONString(pr);
        }

        wearHelmet wearHelmetModel=wearHelmetService.getByWorker(Integer.valueOf(workerId));
        helmet model=helmetService.getById(wearHelmetModel.getHelmetId());
        if(model!=null)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setData(model);
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("头盔不存在");
            pr.setData(null);
            return JSON.toJSONString(pr);
        }
    }
}
