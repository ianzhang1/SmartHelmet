package com.SmartHelmet.Web.APP;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.model.environment;
import com.SmartHelmet.service.EnvironmentService;
import com.SmartHelmet.utils.StringUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.List;
import com.SmartHelmet.config.returnCode;

/**
 * 环境信息
 * Created by Administrator on 2016/12/2.
 */
@Controller
@RequestMapping("/app/api/environment")
public class EnvironmentController {
    @Resource
    EnvironmentService environmentService;

    @ResponseBody
    @RequestMapping(value = "/latest", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getLastByWorker(int workerId) {
        ResponseResult pr=new ResponseResult();
        environment model=environmentService.getLastByWorker(workerId);
        //
        if(model!=null){
            pr.setCode(String.valueOf(returnCode.SUCCESS));
            pr.setData(model);
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND));
            pr.setMessage("没有找到相关环境信息");
            return JSON.toJSONString(pr);
        }
    }


    @ResponseBody
    @RequestMapping(value = "/co", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String geCOByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<environment> result=environmentService.get_CO_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关环境信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/nh3", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_nh3_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<environment> result=environmentService.get_nh3_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关环境信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/co2", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_co2_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<environment> result=environmentService.get_co2_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关环境信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/nox", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_nox_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<environment> result=environmentService.get_nox_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关环境信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/hs", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_hs_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<environment> result=environmentService.get_hs_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关环境信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/o2", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String get_o2_ByDate(String date,int workerId) {
        ResponseResult pr=new ResponseResult();
        List<environment> result=environmentService.get_o2_ByDate(workerId, date);
        if(result!=null)
        {
            pr.setData(result);
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setMessage("没有找到相关环境信息");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }
    }
}
