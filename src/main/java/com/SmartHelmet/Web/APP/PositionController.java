package com.SmartHelmet.Web.APP;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.ViewModel.SceneCanvas;
import com.SmartHelmet.model.position;
import com.SmartHelmet.service.PositionService;
import com.SmartHelmet.utils.StringUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.support.RequestContext;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import com.SmartHelmet.config.returnCode;

import java.util.List;

/**
 * Created by Administrator on 2016/12/9.
 */
@Controller
@RequestMapping("/app/api")
public class PositionController {

    @Resource
    PositionService positionService;


    @ResponseBody
    @RequestMapping(value = "positions", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getPositionByDate(HttpServletRequest request,String date,int workerId)
    {
        RequestContext requestContext = new RequestContext(request);
        String scene_width=requestContext.getMessage("scene_width");
        String scene_length=requestContext.getMessage("scene_length");
        //
        ResponseResult pr=new ResponseResult();
        if(StringUtil.isEmpty(date) || StringUtil.isEmpty(workerId))
        {
            pr.setMessage("缺少必要参数");
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            return JSON.toJSONString(pr);
        }

        List<position> positions=positionService.getPositions(date,workerId);
        if(positions.size()==0)
        {
            pr.setMessage("没有符合条件的数据");
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            return JSON.toJSONString(pr);
        }

        SceneCanvas sc=new SceneCanvas();
        sc.setSceneWidth(Integer.valueOf(scene_width));
        sc.setSceneLength(Integer.valueOf(scene_length));
        sc.setPositions(positions);
        pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
        pr.setData(sc);
        //
        return JSON.toJSONString(pr);
    }
}
