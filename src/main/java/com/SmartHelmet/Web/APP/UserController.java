package com.SmartHelmet.Web.APP;
import com.SmartHelmet.ViewModel.LoginModel;
import com.SmartHelmet.model.master;
import com.SmartHelmet.service.UserService;
import com.SmartHelmet.utils.CryptoUtil;
import com.SmartHelmet.utils.StringUtil;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import com.SmartHelmet.ViewModel.ResponseResult;
import com.alibaba.fastjson.JSON;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import com.SmartHelmet.config.returnCode;

/**
 * Created by Administrator on 2016/11/28.
 */
@Controller
@RequestMapping("/app/api")
public class UserController {

    @Resource
    UserService userService;

    @ResponseBody
    @RequestMapping(value = "login", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String Login(HttpServletRequest request, LoginModel loginModel)
    {
        ResponseResult pr = new ResponseResult();
        master master= userService.login(loginModel);
        if(master!=null) {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("登录成功");
            pr.setData(master);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("登录失败");
            pr.setData(null);
        }
        return JSON.toJSONString(pr);
    }

    @ResponseBody
    @RequestMapping(value = "ChangePassword", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public  String ChangePassword(HttpServletRequest request)
    {
        ResponseResult pr = new ResponseResult();
        String token=request.getHeader("token").toString();
        //
        if(StringUtil.isEmpty(token))
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("token不能为空");
            return JSON.toJSONString(pr);
        }
        //
        String oldPassword=request.getParameter("oldPassword");
        String newPassword=request.getParameter("newPassword");
        String confirmPassword=request.getParameter("confirmPassword");
        //
        master master=userService.verifyToken(token);
        if(master!=null)
        {
            if(!master.getPassword().equalsIgnoreCase(CryptoUtil.MD5(oldPassword)))
            {
                pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
                pr.setMessage("原密码不正确");
                return JSON.toJSONString(pr);
            }

            if(!newPassword.equalsIgnoreCase(confirmPassword)) {
                pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
                pr.setMessage("新密码不一致");
                return JSON.toJSONString(pr);
            }

            master.setPassword(CryptoUtil.MD5(newPassword));
            if(userService.UpdateMaster(master))
            {
                pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
                pr.setMessage("密码更新成功");
                return JSON.toJSONString(pr);
            }
            else
            {
                pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
                pr.setMessage("更新密码时出错");
                return JSON.toJSONString(pr);
            }
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("token无效，没有找到相关账号");
            return JSON.toJSONString(pr);
        }
    }
}
