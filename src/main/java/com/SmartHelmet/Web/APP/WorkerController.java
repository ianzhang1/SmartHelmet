package com.SmartHelmet.Web.APP;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.model.live;
import com.SmartHelmet.model.video;
import com.SmartHelmet.model.worker;
import com.SmartHelmet.service.LiveService;
import com.SmartHelmet.service.VideoService;
import com.SmartHelmet.service.WorkerService;
import com.SmartHelmet.utils.RegexUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import com.SmartHelmet.config.returnCode;

import java.util.List;

/**
 * Created by Administrator on 2016/11/29.
 */
@Controller
@RequestMapping("/app/api")
public class WorkerController {

    @Resource
    WorkerService workerService;

    @Resource
    LiveService liveService;

    @Resource
    VideoService videoService;

    @ResponseBody
    @RequestMapping(value = "/worker", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getWorker(HttpServletRequest request)
    {
        worker worker= workerService.getWorker(Integer.valueOf(request.getParameter("id")));
        ResponseResult pr=new ResponseResult();
        if(worker!=null)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("成功获取");
            pr.setData(worker);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("没有查询到相关信息");
        }
        return JSON.toJSONString(pr);
    }

    @ResponseBody
    @RequestMapping(value = "/workerlist", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String workerList(HttpServletRequest request)
    {
        ResponseResult pr=new ResponseResult();
        if(request.getParameter("page")==null || request.getParameter("pageSize")==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("缺少必要参数");
            return JSON.toJSONString(pr);
        }

        int page=Integer.valueOf(request.getParameter("page"));
        int pageSize=Integer.valueOf(request.getParameter("pageSize"));
        //
        int offset=(page-1)*pageSize;
        List<worker> workers=workerService.getWorkers(offset,pageSize);
        pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
        pr.setData(workers);
        //
        return JSON.toJSONString(pr);
    }

    /**
     * 获取工位的直播信息
     * @param workerId
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "/workerlive", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getLiveingInfoByWorker(HttpServletRequest request)
    {
        ResponseResult pr=new ResponseResult();
        if(request.getParameter("workerId")==null || !RegexUtil.isInteger(request.getParameter("workerId")))
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("参数为空或类型不正确");
            return JSON.toJSONString(pr);
        }

        live result=liveService.getLiveingByWorker((Integer.valueOf(request.getParameter("workerId"))));
        //
        if(result!=null)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setData(result);
            pr.setMessage("");
            return JSON.toJSONString(pr);
        }
        else {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("没有找到该工位的正在直播的数据");
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "/workervideo", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String getVideosInfoByWorker(HttpServletRequest request)
    {
        ResponseResult pr=new ResponseResult();
        if(request.getParameter("page")==null || request.getParameter("pageSize")==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("缺少必要参数");
            return JSON.toJSONString(pr);
        }

        if(request.getParameter("workerId")==null || !RegexUtil.isInteger(request.getParameter("workerId"))
         || request.getParameter("date")==null)
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("参数为空或类型不正确");
            return JSON.toJSONString(pr);
        }

        int workerId=Integer.valueOf(request.getParameter("workerId"));
        String date=String.valueOf(request.getParameter("date"));
        int page=Integer.valueOf(request.getParameter("page"));
        int pageSize=Integer.valueOf(request.getParameter("pageSize"));
        //
        List<video> videos=videoService.getVideos(workerId,date,page,pageSize);
        if(videos!=null && videos.size()>0)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setData(videos);
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("没有找到该工位的点播的数据");
            return JSON.toJSONString(pr);
        }
    }
}
