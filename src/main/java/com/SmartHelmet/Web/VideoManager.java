package com.SmartHelmet.Web;

import com.SmartHelmet.model.video;
import com.SmartHelmet.service.VideoService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ianzhang on 16/12/2.
 */
@Controller
@RequestMapping("/VideoManager")
public class VideoManager {
    private String httpHost="http://114.215.201.40:8080/SmartHelmet";

    @Resource
    VideoService videoService;


    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String List(Model model)
    {
        model.addAttribute("videos",videoService.getAllVideos());
        return "VideoManager/list";
    }

    @RequestMapping(value = "new",method = RequestMethod.GET)
    public String newVideo()
    {
        return "VideoManager/new";
    }

    @RequestMapping(value = "new",method = RequestMethod.POST)
    public String saveNewVideo(@RequestParam(value = "pic_input", required = false) MultipartFile file,HttpServletRequest request, String liveId,String liveDate,String workerId,String videoUrl,String duration,String remark)
    {
        video model=new video();
        //
        model.setCreated(new Date());
        model.setDuration(duration);
        model.setLiveDate(liveDate);
        model.setRemark(remark);
        model.setWorkerId(Integer.valueOf(workerId));
        model.setLiveId(Integer.valueOf(liveId));
        model.setVideoUrl(videoUrl);

        String path = request.getSession().getServletContext().getRealPath("/WEB-INF/uploads/video_screenshot");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String scrennshotFileName = sdf.format(new Date())+file.getOriginalFilename();
        File targetFile = new File(path, scrennshotFileName);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }

        //����
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        String screenshotImgUrl=httpHost+"/uploads/video_screenshot/"+scrennshotFileName;
        model.setScreenshot(screenshotImgUrl);
        //
        videoService.addVideo(model);
        return "redirect:/VideoManager/list";
    }

    @RequestMapping(value = "del/{id}",method = RequestMethod.GET)
    public String del(@PathVariable int id)
    {
        videoService.delVideo(id);
        return "redirect:/VideoManager/list";
    }

    @RequestMapping(value = "modify/{id}",method = RequestMethod.GET)
    public String modify(@PathVariable int id,Model model)
    {
        model.addAttribute("video",videoService.getVideoById(id));
        return "VideoManager/modify";
    }

    @RequestMapping(value = "modify",method = RequestMethod.POST)
    public String modify(@RequestParam(value = "pic_input", required = false) MultipartFile file, HttpServletRequest request,video model)
    {
        if(file.getSize()>0)
        {
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/uploads/video_screenshot");
            SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
            String scrennshotFileName = sdf.format(new Date())+file.getOriginalFilename();
            File targetFile = new File(path, scrennshotFileName);
            if(!targetFile.exists()){
                targetFile.mkdirs();
            }

            //保存
            try {
                file.transferTo(targetFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            model.setScreenshot(httpHost + "/uploads/video_screenshot/" + scrennshotFileName);
        }
        model.setCreated(new Date());
        videoService.updateVideo(model);
        return "redirect:/VideoManager/list";
    }
}
