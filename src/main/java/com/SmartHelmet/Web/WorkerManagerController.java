package com.SmartHelmet.Web;

import com.SmartHelmet.model.helmet;
import com.SmartHelmet.model.worker;
import com.SmartHelmet.service.HelmetService;
import com.SmartHelmet.service.WorkerService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by ianzhang on 16/11/30.
 */
@Controller
@RequestMapping("/WorkerManager")
public class WorkerManagerController {

    private String httpHost="http://114.215.201.40:8080/SmartHelmet";

    @Resource
    WorkerService workerService;

    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String List(Model model)
    {
        model.addAttribute("workers",workerService.getAllWorkers());
        return "WorkerManager/list";
    }


    @RequestMapping(value = "new",method = RequestMethod.GET)
    public String newUser()
    {
        return "WorkerManager/new";
    }

    @RequestMapping(value = "new",method = RequestMethod.POST)
    public String saveNewUser(@RequestParam(value = "pic_input", required = false) MultipartFile file, HttpServletRequest request)
    {
        String path = request.getSession().getServletContext().getRealPath("/WEB-INF/uploads/avaters");
        SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
        String avaterFileName = sdf.format(new Date())+file.getOriginalFilename();
        File targetFile = new File(path, avaterFileName);
        if(!targetFile.exists()){
            targetFile.mkdirs();
        }

        //����
        try {
            file.transferTo(targetFile);
        } catch (Exception e) {
            e.printStackTrace();
        }

        worker model=new worker();
        model.setName(request.getParameter("name"));
        model.setAvater(httpHost+"/uploads/avaters/"+avaterFileName);
        model.setSex(Integer.valueOf(request.getParameter("sex")));
        model.setTypeOfWork(request.getParameter("typeOfWork"));
        model.setDepartmentName(request.getParameter("departmentName"));
        model.setWorkerCode(request.getParameter("workerCode"));
        model.setRemark(request.getParameter("remark"));

        workerService.addNew(model);
        //
        return "redirect:/WorkerManager/list";
    }

    @RequestMapping(value = "del/{id}",method = RequestMethod.GET)
    public String del(@PathVariable int id)
    {
        workerService.deleteWokery(id);
        return "redirect:/WorkerManager/list";
    }


    @RequestMapping(value = "modify/{id}",method = RequestMethod.GET)
    public String modify(@PathVariable int id,Model model)
    {
        model.addAttribute("worker",workerService.getWorker(id));
        return "WorkerManager/modify";
    }

    @RequestMapping(value = "modify",method = RequestMethod.POST)
    public String modify(@RequestParam(value = "pic_input", required = false) MultipartFile file, HttpServletRequest request,worker model)
    {
        if(file!=null)
        {
            String path = request.getSession().getServletContext().getRealPath("/WEB-INF/uploads/avaters");
            SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
            String avaterFileName = sdf.format(new Date())+file.getOriginalFilename();
            File targetFile = new File(path, avaterFileName);
            if(!targetFile.exists()){
                targetFile.mkdirs();
            }

            //����
            try {
                file.transferTo(targetFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
            model.setAvater(httpHost+"/uploads/avaters/"+avaterFileName);
        }
        model.setCreated(new Date());
        workerService.updateWorker(model);
        return "redirect:/WorkerManager/list";
    }
}
