package com.SmartHelmet.Web.Helmet;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.service.LiveService;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import  com.SmartHelmet.config.returnCode;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by ianzhang on 16/12/3.
 */
@Controller
@RequestMapping("/Helmet/Live")
public class LiveController {
    @Resource
    LiveService liveService;

    /**
     * 设备开始直播状态
     * @param request
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "start",method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String start(HttpServletRequest request)
    {
        ResponseResult pr=new ResponseResult();
        int liveId=Integer.valueOf(request.getParameter("liveId"));
        int status=Integer.valueOf(request.getParameter("status"));
        //
        boolean result=liveService.updateLiveStatus(liveId,status);
        if(result)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("操作成功");
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("操作失败");
            return JSON.toJSONString(pr);
        }
    }

    @ResponseBody
    @RequestMapping(value = "end",method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String end(HttpServletRequest request)
    {
        ResponseResult pr=new ResponseResult();
        //
        int liveId=Integer.valueOf(request.getParameter("liveId"));
        int status=Integer.valueOf(request.getParameter("status"));
        //
        liveService.updateLiveStatus(liveId,status);
        boolean result=liveService.updateLiveStatus(liveId,status);
        if(result)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("操作成功");
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("操作失败");
            return JSON.toJSONString(pr);
        }
    }
}
