package com.SmartHelmet.Web.Helmet;

import com.SmartHelmet.ViewModel.ResponseResult;
import com.SmartHelmet.model.*;
import com.SmartHelmet.service.*;
import com.SmartHelmet.utils.StringUtil;
import com.alibaba.fastjson.JSON;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import com.SmartHelmet.config.returnCode;
import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.Date;

/**
 * 头盔采集相关接口
 */
@Controller
@RequestMapping("/Helmet")
public class collectController {

    @Resource
    HelmetService helmetService;

    @Resource
    VitalSignsService VitalSignsService;

    @Resource
    WearHelmetService wearHelmetService;

    @Resource
    EnvironmentService environmentService;

    @Resource
    PositionService positionService;

    /**
     * 采集头盔剩余电量和工作时间
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "collectHelmetInfo", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String collectHelmetInfo(helmet helmet)
    {
        ResponseResult pr=new ResponseResult();
        if(StringUtil.isEmpty(helmet.getCode()) ||
                StringUtil.isEmpty(helmet.getResidualElectricity()) ||
                StringUtil.isEmpty(helmet.getWorkingTime()))
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("必要的参数不能为空");
            return JSON.toJSONString(pr);
        }

        //
        helmet model=helmetService.getByCode(helmet.getCode());
        if(model!=null)
        {
            model.setResidualElectricity(helmet.getResidualElectricity());
            model.setWorkingTime(helmet.getWorkingTime());
            //
            if(helmetService.updateHelmet(model)) {
                pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
                pr.setMessage("头盔信息更新成功");
                pr.setData(null);
                return JSON.toJSONString(pr);
            }
        }
        //
        pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
        pr.setMessage("操作过程出现错误或头盔不存在");
        pr.setData(null);
        return JSON.toJSONString(pr);
    }

    /**
     * 采集生命体征数据
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "collectVitalSigns", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String collectVitalSigns(String helmetCode,String heartRate,String temperature,String bloodPressure)
    {
        ResponseResult pr=new ResponseResult();
        helmet helmetModel=helmetService.getByCode(helmetCode);
        if(helmetModel==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("头盔序列号没有找到");
            return JSON.toJSONString(pr);
        }

        worker workerModel=wearHelmetService.getWorkerByHelmet(helmetModel.getId());
        if(workerModel==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("该头盔序列号没有分配给任何工人");
            return JSON.toJSONString(pr);
        }

        vitalSigns model=new vitalSigns();
        model.setCreated(new Date());
        model.setHelmetId(helmetModel.getId());
        model.setBloodPressure(bloodPressure);
        model.setHeartRate(heartRate);
        model.setTemperature(temperature);
        model.setWorkerId(workerModel.getId());
        boolean result=VitalSignsService.add(model);
        //
        if(result)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("采集成功");
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("采集失败");
            return JSON.toJSONString(pr);
        }
    }

    /**
     * 采集环境信息
     * @param helmetCode
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "collectEnvironment", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String collectEnvironment(String helmetCode,String co,String nh3,String co2,String nox,String hs,String o2)
    {
        environment model=new environment();
        ResponseResult pr=new ResponseResult();
        helmet helmetModel=helmetService.getByCode(helmetCode);
        if(helmetModel==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("头盔序列号没有找到");
            return JSON.toJSONString(pr);
        }

        worker workerModel=wearHelmetService.getWorkerByHelmet(helmetModel.getId());
        if(workerModel==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("该头盔序列号没有分配给任何工人");
            return JSON.toJSONString(pr);
        }
        model.setWorkerId(workerModel.getId());
        model.setCreated(new Date());
        model.setHelmetId(helmetModel.getId());
        model.setCo(co);
        model.setCo2(co2);
        model.setHs(hs);
        model.setNh3(nh3);
        model.setNox(nox);
        model.setO2(o2);
        //
        boolean result=environmentService.add(model);
        if(result)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("采集成功");
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("采集失败");
            return JSON.toJSONString(pr);
        }
    }

    /**
     * 头盔方位信息采集
     * @return
     */
    @ResponseBody
    @RequestMapping(value = "collectPosition", method = {RequestMethod.POST },produces="text/html;charset=UTF-8")
    public String collectPosition(String helmetCode,String x,String y,String z)
    {
        ResponseResult pr=new ResponseResult();
        if(StringUtil.isEmpty(helmetCode) || StringUtil.isEmpty(x) || StringUtil.isEmpty(y) || StringUtil.isEmpty(y))
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("缺少必要参数");
            return JSON.toJSONString(pr);
        }

        helmet helmetModel=helmetService.getByCode(helmetCode);
        if(helmetModel==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("头盔序列号没有找到");
            return JSON.toJSONString(pr);
        }

        worker workerModel=wearHelmetService.getWorkerByHelmet(helmetModel.getId());
        if(workerModel==null)
        {
            pr.setCode(String.valueOf(returnCode.NOT_FOUND.ordinal()));
            pr.setMessage("该头盔序列号没有分配给任何工人");
            return JSON.toJSONString(pr);
        }

        position positionModel=new position();
        positionModel.setCreated(new Date());
        positionModel.setHelmetId(helmetModel.getId());
        positionModel.setWorkerId(workerModel.getId());
        positionModel.setX(x);
        positionModel.setY(y);
        positionModel.setZ(z);
        boolean result=positionService.addPosition(positionModel);
        if(result)
        {
            pr.setCode(String.valueOf(returnCode.SUCCESS.ordinal()));
            pr.setMessage("采集成功");
            return JSON.toJSONString(pr);
        }
        else
        {
            pr.setCode(String.valueOf(returnCode.ERROR.ordinal()));
            pr.setMessage("采集失败");
            return JSON.toJSONString(pr);
        }
    }
}
