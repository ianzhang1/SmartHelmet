package com.SmartHelmet.Web;

import com.SmartHelmet.service.WorkerService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import javax.annotation.Resource;

@Controller
@RequestMapping("/home")
public class HomeController {

	@RequestMapping(value="/index",method = RequestMethod.GET)
	public String printWelcome(ModelMap model) {
		return "index";
	}
}