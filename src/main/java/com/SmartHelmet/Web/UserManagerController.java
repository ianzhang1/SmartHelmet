package com.SmartHelmet.Web;

import com.SmartHelmet.model.master;
import com.SmartHelmet.service.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.annotation.Resource;
import com.SmartHelmet.utils.CryptoUtil;

import java.util.Date;

/**
 * Created by Administrator on 2016/11/30.
 */
@Controller
@RequestMapping("/UserManager")
public class UserManagerController {
    @Resource
    UserService userService;

    @RequestMapping(value = "list",method = RequestMethod.GET)
    public String List(Model model)
    {
        model.addAttribute("users",userService.getAllMaster());
        return "UserManager/list";
    }


    @RequestMapping(value = "new",method = RequestMethod.GET)
    public String newUser()
    {
        return "UserManager/new";
    }

    @RequestMapping(value = "new",method = RequestMethod.POST)
    public String saveNewUser(master model)
    {
        model.setPassword(CryptoUtil.MD5(model.getPassword()));
        userService.newMaster(model);
        //
        return "redirect:/UserManager/list";
    }

    @RequestMapping(value = "del/{id}",method = RequestMethod.GET)
    public String del(@PathVariable int id)
    {
        userService.deleteUser(id);
        return "redirect:/UserManager/list";
    }

    @RequestMapping(value = "modify/{id}",method = RequestMethod.GET)
    public String modify(@PathVariable int id,Model model)
    {
        model.addAttribute("user", userService.getUser(id));
        return "UserManager/modify";
    }

    @RequestMapping(value = "modify",method = RequestMethod.POST)
    public String saveModifyUser(master model)
    {
        model.setCreated(new Date());
        userService.UpdateMaster(model);
        return "redirect:/UserManager/list";
    }
}
