package com.SmartHelmet.dao;

import com.SmartHelmet.model.environment;

import java.util.HashMap;
import java.util.List;

public interface environmentMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(environment record);

    int insertSelective(environment record);

    environment selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(environment record);

    int updateByPrimaryKey(environment record);

    List<environment> getListByDate(HashMap map);

    environment getLastByWorker(int workerId);

    List<environment> get_CO_ByDate(HashMap map);

    List<environment> get_nh3_ByDate(HashMap map);

    List<environment> get_co2_ByDate(HashMap map);

    List<environment> get_nox_ByDate(HashMap map);

    List<environment> get_hs_ByDate(HashMap map);

    List<environment> get_o2_ByDate(HashMap map);
}