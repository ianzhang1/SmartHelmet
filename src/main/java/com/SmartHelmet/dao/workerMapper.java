package com.SmartHelmet.dao;

import com.SmartHelmet.model.worker;

import java.util.List;
import java.util.Map;

public interface workerMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(worker record);

    int insertSelective(worker record);

    worker selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(worker record);

    int updateByPrimaryKey(worker record);

    List<worker> selectAll();

    List<worker> getWorkersByPage(Map map);
}