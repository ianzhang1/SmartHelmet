package com.SmartHelmet.dao;

import com.SmartHelmet.model.vitalSigns;

import java.util.HashMap;
import java.util.List;

public interface vitalSignsMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(vitalSigns record);

    int insertSelective(vitalSigns record);

    vitalSigns selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(vitalSigns record);

    int updateByPrimaryKey(vitalSigns record);

    vitalSigns getLastInfoByWorker(int workerId);

    List<vitalSigns> get_heart_rateByDate(HashMap map);

    List<vitalSigns> get_temperature_ByDate(HashMap map);

    List<vitalSigns> get_blood_pressure_ByDate(HashMap map);
}