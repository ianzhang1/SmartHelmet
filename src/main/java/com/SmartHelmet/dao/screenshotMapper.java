package com.SmartHelmet.dao;

import com.SmartHelmet.model.screenshot;

public interface screenshotMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(screenshot record);

    int insertSelective(screenshot record);

    screenshot selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(screenshot record);

    int updateByPrimaryKey(screenshot record);
}