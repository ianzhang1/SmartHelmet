package com.SmartHelmet.dao;

import com.SmartHelmet.model.video;

import java.util.HashMap;
import java.util.List;

public interface videoMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(video record);

    int insertSelective(video record);

    video selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(video record);

    int updateByPrimaryKey(video record);

    List<video> selectAll();

    List<video> selectByDate(HashMap<String,Object> map);
}