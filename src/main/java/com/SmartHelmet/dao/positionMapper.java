package com.SmartHelmet.dao;

import com.SmartHelmet.model.position;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface positionMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(position record);

    int insertSelective(position record);

    position selectByPrimaryKey(Integer id);

    List<position> selectByDateAndWorkerId(Map map);

    int updateByPrimaryKeySelective(position record);

    int updateByPrimaryKey(position record);
}