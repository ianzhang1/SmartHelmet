package com.SmartHelmet.dao;

import com.SmartHelmet.model.token;

public interface tokenMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(token record);

    int insertSelective(token record);

    token selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(token record);

    int updateByPrimaryKey(token record);

    token getLastTokenModel(String token);
}