package com.SmartHelmet.dao;

import com.SmartHelmet.model.wearHelmet;

import java.util.List;

public interface wearHelmetMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(wearHelmet record);

    int insertSelective(wearHelmet record);

    wearHelmet selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(wearHelmet record);

    int updateByPrimaryKey(wearHelmet record);

    wearHelmet selectByWorker(int workerId);

    wearHelmet selectByHelmet(int helmetId);

    List<wearHelmet> selectAll();
}