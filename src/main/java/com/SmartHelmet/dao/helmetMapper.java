package com.SmartHelmet.dao;

import com.SmartHelmet.model.helmet;

import java.util.List;

public interface helmetMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(helmet record);

    int insertSelective(helmet record);

    helmet selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(helmet record);

    int updateByPrimaryKey(helmet record);

    helmet selectByCode(String code);

    List<helmet> selectAll();
}