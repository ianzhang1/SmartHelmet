package com.SmartHelmet.dao;

import com.SmartHelmet.model.live;

import java.util.HashMap;
import java.util.List;

public interface liveMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(live record);

    int insertSelective(live record);

    live selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(live record);

    int updateByPrimaryKey(live record);

    List<live> selectAll();

    List<live> selectByStatus(int status);

    int updateStatus(HashMap map);

    live selectLiveingByWorker(Integer workerId);
}