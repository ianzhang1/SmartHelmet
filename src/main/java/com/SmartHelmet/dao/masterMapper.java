package com.SmartHelmet.dao;

import com.SmartHelmet.ViewModel.LoginModel;
import com.SmartHelmet.model.master;

import java.util.List;

public interface masterMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(master record);

    int insertSelective(master record);

    master selectByPrimaryKey(Integer id);

    /***
     * ��¼��֤
     * @param model
     * @return
     */
    master selectByUserNameAndPWD(LoginModel model);

    int updateByPrimaryKeySelective(master record);

    int updateByPrimaryKey(master record);

    master selectByToken(String token);

    List<master> selectAllMaster();

    master selectByEmail(String email);
}