<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.definition">
    <tiles:putAttribute name="body">
        <div class="breadcrumbs" id="breadcrumbs">
            <script type="text/javascript">
                try {
                    ace.settings.check('breadcrumbs', 'fixed')
                } catch (e) {
                }
            </script>
            <ul class="breadcrumb">
                <li><i class="icon-home home-icon"></i> <a href="#">首页</a></li>

                <li><a href="/SmartHelmet/UserManager/list">APP用户管理</a></li>
            </ul>
            <!-- .breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <div class="table-responsive">
                        <div id="sample-table-2_wrapper" class="dataTables_wrapper"
                             role="grid">
                            <div class="row">
                                <div class="col-sm-6">
                                    <a href="/SmartHelmet/UserManager/new"
                                       class="btn btn-info btn-yellow btn-xs"> <i class="icon-edit bigger-100"></i> 新增
                                    </a>
                                </div>
                                <%--<div class="col-sm-6">--%>
                                    <%--<form method="get" action="admin/news/getList"--%>
                                          <%--id="form_query">--%>
                                        <%--<div class="dataTables_filter" id="sample-table-2_filter">--%>
                                            <%--<label>关键字: <input type="text" id="keyword"--%>
                                                               <%--value="<?php echo !empty($_GET['keyword'])?$_GET['keyword']:''?>"--%>
                                                               <%--name="keyword" aria-controls="sample-table-2"></label> <a--%>
                                                <%--href="javascript://" onclick="$('#form_query').submit();"--%>
                                                <%--class="btn btn-info btn-xs"> <i class="icon-search bigger-100"></i>--%>
                                            <%--查询--%>
                                        <%--</a>--%>
                                        <%--</div>--%>
                                    <%--</form>--%>
                                <%--</div>--%>
                            </div>
                            <table id="tbl_list"
                                   class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>EMAIL</th>
                                    <th>姓名</th>
                                    <th>电话</th>
                                    <th>操作</th>
                                </tr>
                                </thead>
                                <tbody role="alert" aria-live="polite" aria-relevant="all">

                                <c:forEach var="user" items="${users}">
                                    <tr class="odd">
                                        <td class="">${user.id}</td>
                                        <td class="">${user.email}
                                        </td>
                                        <td>${user.name} </td>
                                        <td>${user.phone} </td>
                                        <td>
                                            <a href="/SmartHelmet/UserManager/del/${user.id}" onclick="return confirm('确定要删除该数据吗？')">删除</a>-<a href="/SmartHelmet/UserManager/modify/${user.id}">修改</a>
                                        </td>
                                    </tr>
                                </c:forEach>
                                </tbody>
                            </table>
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="dataTables_info" id="sample-table-2_info">共<c:out value="${users.size()}"/>条</div>
                                </div>
                                <div class="col-sm-6">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.page-content -->
        <script type="text/javascript">
            $(function () {
                $('#tbl_list').dataTable({
                    "bInfo": false,
                    "bLengthChange": false,
                    "bPaginate": false, //翻页功能
                    "bFilter": false, //过滤功能
                    "aoColumns": [
                        null,
                        null, null, null,
                        {"bSortable": false}
                    ]
                });

            });
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>