<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<tiles:insertDefinition name="base.definition">
  <tiles:putAttribute name="body">
    <div class="breadcrumbs" id="breadcrumbs">
      <ul class="breadcrumb">
        <li><i class="icon-home home-icon"></i> <a href="#">首页</a></li>
        <li><a href="/SmartHelmet/UserManager/list">APP用户列表</a></li>
        <li class="active">修改用户</li>
      </ul>
      <!-- .breadcrumb -->
    </div>
    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <form class="form-horizontal" role="form" method="post" action="/SmartHelmet/UserManager/modify">
            <input type="hidden" id="id" name="id" value="${user.id}"/>

            <div class="form-group">
              <label for="email" class="col-sm-2 control-label field_name not_null">Email：</label>
              <div class="col-sm-4">
                <input class="form-control" id="email" name='email' value="${user.email}" placeholder="Email"/>
              </div>
            </div>
            <div class="form-group">
              <label for="name" class="col-sm-2 control-label field_name not_null">姓名：</label>
              <div class="col-sm-4">
                <input class="form-control" id="name" value="${user.name}" name='name' placeholder="姓名"/>
              </div>
            </div>
            <div class="form-group">
              <label for="phone" class="col-sm-2 control-label field_name not_null">电话：</label>
              <div class="col-sm-4">
                <input class="form-control" id="phone" value="${user.phone}" name='phone' placeholder="电话"/>
              </div>
            </div>
            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">确定</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </tiles:putAttribute>
</tiles:insertDefinition>