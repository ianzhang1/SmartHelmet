<%--
  Created by IntelliJ IDEA.
  User: ianzhang
  Date: 16/11/30
  Time: 下午10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.definition">
    <tiles:putAttribute name="body">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li><i class="icon-home home-icon"></i> <a href="#">首页</a></li>
                <li><a href="/SmartHelmet/LiveManager/list">直播列表</a></li>
                <li class="active">添加直播</li>
            </ul>
            <!-- .breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-horizontal" role="form" method="post"  enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="title" class="col-sm-2 control-label field_name not_null">标题：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="title" name='title' placeholder="标题"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="pushUrl" class="col-sm-2 control-label field_name not_null">推流地址：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="pushUrl" name='pushUrl' placeholder="推流地址"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="httpPullUrl" class="col-sm-2 control-label field_name not_null">拉流地址(HTTP)：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="httpPullUrl" name='httpPullUrl' placeholder="拉流地址(HTTP)"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="hlsPullUrl" class="col-sm-2 control-label field_name not_null">拉流地址(HLS)：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="hlsPullUrl" name='hlsPullUrl' placeholder="拉流地址(HLS)"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="rtmpPullUrl" class="col-sm-2 control-label field_name not_null">拉流地址(RTMP)：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="rtmpPullUrl" name='rtmpPullUrl' placeholder="拉流地址(RTMP)"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="workerId" class="col-sm-2 control-label field_name not_null">工位ID：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="workerId" name='workerId' placeholder="工位ID"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="helmentId" class="col-sm-2 control-label field_name not_null">头盔ID：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="helmentId" name='helmentId' placeholder="头盔ID"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="liveImg" class="col-sm-2 control-label field_name not_null">视频截图：</label>
                            <div class="col-sm-4">
                                <div style="display: block" id="pic_display">
                                    <img alt="" src="" id="img_preview">
                                </div>
                                <div id="pic_content_frm">
                                    <span onclick="$('#pic_input').click();" class="btn btn-info select_pic_btn" id="pic_content">上传图片</span>
                                    <input type="file" id="pic_input" class="select_btn" name='pic_input' onchange="fileHandler(this);" style="opacity: 0;"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="remark" class="col-sm-2 control-label field_name not_null">备注：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="remark" name='remark' placeholder="备注"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">确定</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <script type="application/javascript">
            window.URL = window.URL || window.webkitURL;
            function fileHandler(obj){
                var files =  obj.files;
                var img = new Image();
                if (window.URL) {
                    if(files[0].size > 2 * 1024 * 1024){
                        alert("图片不能超过2M！请重新选择");
                    }else{
                        var reader = new FileReader();
                        reader.readAsDataURL(files[0]);
                        reader.onload = function(e) {
                            img.src = this.result;
                        }

                        id=obj.id;
                        if(id=='pic_input'){
                            img.onload = function(){
                                $("#pic_display").css('display', 'none');
                                $("#submit").css('display', 'block');
                                $("#pic_content").removeClass("btn btn-info");
                                $("#pic_input").width(img.width);
                                $("#pic_input").hide();
                                $("#pic_content").width(img.width);
                                $("#pic_content").height(img.height);

                                $("#pic_content_frm").removeClass("btn btn-info");
                                $("#pic_content_frm").width(img.width);
                                $("#pic_content_frm").height(img.height);
                                $("#pic_content").html(img);
                            }
                        }
                        else
                        {
                            img.onload=function(){
                                $("#pic_extend_image_content").removeClass("btn btn-info");
                                $("#pic_extend_image_content").width(img.width);
                                $("#pic_extend_image_content").height(img.height);

                                $("#pic_extend_content_frm").removeClass("btn btn-info");
                                $("#pic_extend_content_frm").width(img.width);
                                $("#pic_extend_content_frm").height(img.height);

                                $("#pic_extend_image_input").hide();
                                $("#pic_extend_image_content").html(img);
                            }
                        }
                    }
                }
            }
        </script>
    </tiles:putAttribute>
</tiles:insertDefinition>