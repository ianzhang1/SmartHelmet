<%--
  Created by IntelliJ IDEA.
  User: ianzhang
  Date: 16/11/30
  Time: 下午10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.definition">
    <tiles:putAttribute name="body">
        <div class="breadcrumbs" id="breadcrumbs">
            <ul class="breadcrumb">
                <li><i class="icon-home home-icon"></i> <a href="#">首页</a></li>
                <li><a href="/SmartHelmet/HelmetManager/list">头盔列表</a></li>
                <li class="active">添加头盔</li>
            </ul>
            <!-- .breadcrumb -->
        </div>
        <div class="page-content">
            <div class="row">
                <div class="col-xs-12">
                    <form class="form-horizontal" role="form" method="post">
                        <div class="form-group">
                            <label for="code" class="col-sm-2 control-label field_name not_null">序列号：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="code" name='code' placeholder="序列号"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="residualElectricity" class="col-sm-2 control-label field_name not_null">剩余电量：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="residualElectricity" name='residualElectricity' placeholder="剩余电量"/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="workingTime" class="col-sm-2 control-label field_name not_null">工作时长：</label>
                            <div class="col-sm-4">
                                <input class="form-control" id="workingTime" name='workingTime' placeholder="工作时长"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">确定</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </tiles:putAttribute>
</tiles:insertDefinition>