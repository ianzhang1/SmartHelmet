<%--
  Created by IntelliJ IDEA.
  User: ianzhang
  Date: 16/11/30
  Time: 下午10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>

<tiles:insertDefinition name="base.definition">
  <tiles:putAttribute name="body">
    <div class="breadcrumbs" id="breadcrumbs">
      <ul class="breadcrumb">
        <li><i class="icon-home home-icon"></i> <a href="#">首页</a></li>
        <li><a href="/SmartHelmet/Wear/list">配戴列表</a></li>
        <li class="active">修改配戴</li>
      </ul>
      <!-- .breadcrumb -->
    </div>
    <div class="page-content">
      <div class="row">
        <div class="col-xs-12">
          <div class="alert alert-danger" style="<c:if test="${err}">display: block;</c:if><c:if test="${err==null}">display: none;</c:if>">
            <button type="button" class="close" data-dismiss="alert">
              <i class="ace-icon fa fa-times"></i>
            </button>
            <i class="ace-icon fa fa-check green"></i>
              ${err}
          </div>

          <form class="form-horizontal" role="form" method="post" action="/SmartHelmet/Wear/modify/">
            <div class="form-group">
              <label for="code" class="col-sm-2 control-label field_name not_null">头盔序列号：</label>
              <div class="col-sm-4">
                <input class="form-control" id="code" value="${wear.helmet.code}" name='code' placeholder="头盔序列号"/>
              </div>
            </div>
            <div class="form-group">
              <label for="workerId" class="col-sm-2 control-label field_name not_null">工位ID：</label>
              <div class="col-sm-4">
                <input class="form-control" id="workerId" value="${wear.workerId}" name='workerId' placeholder="工位ID"/>
              </div>
            </div>

            <div class="form-group">
              <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-default">确定</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </tiles:putAttribute>
</tiles:insertDefinition>