#app接口部分

##1.app登录接口
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/login
- **参数**

>email,账号
>password,密码

- **返回值**

>{
     "code": "0",
     "data": {
         "created": 1480262400000,
         "email": "admin",
         "id": 1,
         "name": "李四",
         "password": "4297f44b13955235245b2497399d7a93",
         "phone": "13122222234332",
         "status": 1,
         "token": "acb59b7f-0c4d-468d-8956-329453611735"
     },
     "message": "登录成功"
 }
- **其它**

>code,0表示成功；1表示未找到；2表示出错
>message:相关描述信息

##2.更新密码接口
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/ChangePassword

- **参数**

>oldPassword，原密码
>newPassword，新密码
>confirmPassword,确认密码


- **返回值**

>{"code":"0","message":"密码更新成功"}

- **其它**


##3.获取指定工位信息
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/worker

- **参数**

>id,工位id(现场人员id)


- **返回值**

>{
     "code": "0",
     "data": {
         "avater": "1",
         "departmentName": "售前部",
         "id": 1,
         "name": "张三",
         "remark": "1",
         "sex": 1,
         "typeOfWork": "水泥工",
         "workerCode": "10021"
     },
     "message": "成功获取"
 }

- **其它**

>返回值说明
>avater,头像,http格式地址
>sex,0男；1女
>typeOfWork，工种描述
>departmentName,所属部门
>workerCode,工号



##4.获取所有工位列表
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/workerlist

- **参数**

>page,页数，从1开始
>pageSize,每页条数

- **返回值**

>{
     "code": "0",
     "data": [
         {
             "avater": "http://114.215.201.40:8080/SmartHelmet/uploads/avaters/20161201172438下载.jpg",
             "departmentName": "NBA",
             "id": 10,
             "name": "景茵梦",
             "remark": "身心正常",
             "sex": 0,
             "typeOfWork": "中锋",
             "workerCode": "1002143"
         },
         {
             "avater": "http://114.215.201.40:8080/SmartHelmet/uploads/avaters/20161201172419u=2748723533,3040180967&fm=23&gp=0.jpg",
             "departmentName": "开发部",
             "id": 9,
             "name": "李灵黛",
             "remark": "身心正常",
             "sex": 1,
             "typeOfWork": "中锋",
             "workerCode": "1002141"
         },
         {
             "avater": "http://114.215.201.40:8080/SmartHelmet/uploads/avaters/20161201172248下载.jpg",
             "departmentName": "NBA",
             "id": 8,
             "name": "王明远",
             "remark": "身心正常",
             "sex": 0,
             "typeOfWork": "工种信息",
             "workerCode": "1002145"
         }
     ]
 }

- **其它**
http post方式


##5.采集头盔信息(剩余电量、工作时长)
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/Helmet/collectHelmetInfo

- **参数**

>code，头盔编号，类型：文本
>residualElectricity，剩余电量，类型：文本
>workingTime,工作时长，类型：文本

- **返回值**

>{"code":"0","message":"头盔信息更新成功"}

- **其它**

>提交方式：post
>code,0成功；1未找到；2出错




##6.获取指定工位正在直播数据
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/workerlive

- **参数**

>workerId,工位id

- **返回值**

>有数据时返回如下：
>{
     "code": "0",
     "data": {
         "created": 1480646616000,
         "helmentId": 13,
         "hlsPullUrl": "http://pullhlsdl471b1cba.live.126.net/live/5555369202544f0895e31819e8116f8a/playlist.m3u8",
         "httpPullUrl": "http://flvdl471b1cba.live.126.net/live/5555369202544f0895e31819e8116f8a.flv?netease=flvdl471b1cba.live.126.net",
         "id": 3,
         "liveImg": "http://114.215.201.40:8080/SmartHelmet/uploads/live_screenshot/20161202104336u=1293775107,6809434&fm=23&gp=0.jpg",
         "pushUrl": "rtmp://pdl471b1cba.live.126.net/live/5555369202544f0895e31819e8116f8a?wsSecret=5f4ecf5e69503ba897f528dd62dadc63&wsTime=1480570510",
         "remark": "12点准时开始",
         "rtmpPullUrl": "rtmp://vdl471b1cba.live.126.net/live/5555369202544f0895e31819e8116f8a",
         "status": 0,
         "title": "演唱会直播2016",
         "workerId": 10
     },
     "message": ""
 }

>无数据时返回如下:
>{"code":"1","message":"没有找到该工位的正在直播的数据"}

- **其它**

>提交方式 post


##7.采集生命体征数据
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/Helmet/collectVitalSigns

- **参数**

>helmetCode,头盔序列号
>heartRate,心率
>temperature,体温
>bloodPressure,血压

- **返回值**

>成功采集
>{"code":"0","message":"采集成功"}

>未找到
>{"code":"1","message":"头盔序列号没有找到"}

>未佩戴
>{"code":"1","message":"该头盔序列号没有分配给任何工人"}

- **其它**

>提交方式,HTTP POST



##8.采集环境数据
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/Helmet/collectEnvironment

- **参数**

>helmetCode,头盔序列号
>co
>nh3
>co2
>nox
>hs
>o2

- **返回值**

>成功采集
>{"code":"0","message":"采集成功"}

>未找到
>{"code":"1","message":"头盔序列号没有找到"}

>未佩戴
>{"code":"1","message":"该头盔序列号没有分配给任何工人"}

- **其它**

>提交方式,HTTP POST


##9.获取指定工位，指定日期的环境数据
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/environment/co
>http://114.215.201.40:8080/SmartHelmet/app/api/environment/nh3
>http://114.215.201.40:8080/SmartHelmet/app/api/environment/co2
>http://114.215.201.40:8080/SmartHelmet/app/api/environment/nox
>http://114.215.201.40:8080/SmartHelmet/app/api/environment/hs
>http://114.215.201.40:8080/SmartHelmet/app/api/environment/o2


- **参数**

>workerId，工位编号
>date,字符串形式，格式：'2016-12-02'

- **返回值**
以此接口为例返回值如下
>http://114.215.201.40:8080/SmartHelmet/app/api/environment/co
>{
     "code": "0",
     "data": [
         {
             "created": 1480608000000,
             "o2": "0"
         },
         {
             "created": 1480609800000,
             "helmetId": 13,
             "id": 1,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480610580000,
             "helmetId": 13,
             "id": 3,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480610580000,
             "helmetId": 13,
             "id": 2,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480611600000,
             "helmetId": 13,
             "id": 4,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480612320000,
             "helmetId": 13,
             "id": 5,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480613400000,
             "helmetId": 13,
             "id": 6,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480614180000,
             "helmetId": 13,
             "id": 7,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480615200000,
             "helmetId": 13,
             "id": 8,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480615980000,
             "helmetId": 13,
             "id": 9,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480616100000,
             "helmetId": 13,
             "id": 10,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480617000000,
             "helmetId": 13,
             "id": 11,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480618800000,
             "helmetId": 13,
             "id": 12,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480620180000,
             "helmetId": 13,
             "id": 13,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480620600000,
             "helmetId": 13,
             "id": 14,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480622400000,
             "helmetId": 13,
             "id": 15,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480624200000,
             "helmetId": 13,
             "id": 18,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480624200000,
             "helmetId": 13,
             "id": 17,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480624200000,
             "helmetId": 13,
             "id": 16,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480626000000,
             "helmetId": 13,
             "id": 21,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480626000000,
             "helmetId": 13,
             "id": 20,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480626000000,
             "helmetId": 13,
             "id": 19,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480627800000,
             "helmetId": 13,
             "id": 23,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480627800000,
             "helmetId": 13,
             "id": 22,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480629600000,
             "helmetId": 13,
             "id": 24,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480631400000,
             "helmetId": 13,
             "id": 25,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480633200000,
             "o2": "0"
         },
         {
             "created": 1480635000000,
             "o2": "0"
         },
         {
             "created": 1480636800000,
             "o2": "0"
         },
         {
             "created": 1480638600000,
             "o2": "0"
         },
         {
             "created": 1480640400000,
             "o2": "0"
         },
         {
             "created": 1480642200000,
             "helmetId": 13,
             "id": 26,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480644000000,
             "helmetId": 13,
             "id": 27,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480645800000,
             "o2": "0"
         },
         {
             "created": 1480647600000,
             "helmetId": 13,
             "id": 28,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480649400000,
             "o2": "0"
         },
         {
             "created": 1480651200000,
             "o2": "0"
         },
         {
             "created": 1480653000000,
             "o2": "0"
         },
         {
             "created": 1480654800000,
             "o2": "0"
         },
         {
             "created": 1480656600000,
             "o2": "0"
         },
         {
             "created": 1480658400000,
             "o2": "0"
         },
         {
             "created": 1480660200000,
             "o2": "0"
         },
         {
             "created": 1480662000000,
             "o2": "0"
         },
         {
             "created": 1480663800000,
             "o2": "0"
         },
         {
             "created": 1480664280000,
             "helmetId": 13,
             "id": 78,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664280000,
             "helmetId": 13,
             "id": 71,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664280000,
             "helmetId": 13,
             "id": 64,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664280000,
             "helmetId": 13,
             "id": 57,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 84,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 83,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 82,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 81,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 80,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 79,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 77,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 76,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 75,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 74,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 73,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 72,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 70,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 69,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 68,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 67,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 66,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 65,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 63,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 62,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 61,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 60,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 59,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480664580000,
             "helmetId": 13,
             "id": 58,
             "o2": "6",
             "workerId": 10
         },
         {
             "created": 1480665600000,
             "o2": "0"
         },
         {
             "created": 1480667400000,
             "o2": "0"
         },
         {
             "created": 1480669200000,
             "o2": "0"
         },
         {
             "created": 1480671000000,
             "o2": "0"
         },
         {
             "created": 1480672800000,
             "o2": "0"
         },
         {
             "created": 1480674600000,
             "o2": "0"
         },
         {
             "created": 1480676400000,
             "o2": "0"
         },
         {
             "created": 1480678200000,
             "o2": "0"
         },
         {
             "created": 1480680000000,
             "o2": "0"
         },
         {
             "created": 1480681800000,
             "o2": "0"
         },
         {
             "created": 1480683600000,
             "o2": "0"
         },
         {
             "created": 1480685400000,
             "o2": "0"
         },
         {
             "created": 1480687200000,
             "o2": "0"
         },
         {
             "created": 1480689000000,
             "o2": "0"
         },
         {
             "created": 1480690800000,
             "o2": "0"
         },
         {
             "created": 1480692600000,
             "o2": "0"
         }
     ]
 }

- **其它**

>返回一天内48个时间点环境数据（以半小时为粒度）


##10.获取指定工位最新的环境信息
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/environment/latest

- **参数**

>workerId，工位编号

- **返回值**

>{
      "code": "SUCCESS",
      "data": {
          "co": "1",
          "co2": "3",
          "created": 1480664582000,
          "helmetId": 13,
          "hs": "5",
          "id": 2,
          "nh3": "2",
          "nox": "4",
          "o2": "6",
          "workerId": 10
      }
  }

- **其它**



##11.获取指定工位最新的生命体征信息
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/vitalSigns/latest

- **参数**

>workerId,工位编号

- **返回值**

>{
     "code": "0",
     "data": {
         "bloodPressure": "90-120",
         "created": 1480663053000,
         "heartRate": "12",
         "helmetId": 13,
         "id": 2,
         "temperature": "36.6",
         "workerId": 10
     }
 }

- **其它**

>返回值说明
>bloodPressure，血压
>heartRate，心率
>temperature,体温



##12.获取指定工位，指定日期的生命体征数据
- **调用地址**

>心率
>http://114.215.201.40:8080/SmartHelmet/app/api/vitalSigns/heartrate
>体温
>http://114.215.201.40:8080/SmartHelmet/app/api/vitalSigns/temperature
>血压
>http://114.215.201.40:8080/SmartHelmet/app/api/vitalSigns/bloodpressure




- **参数**

>workerId，工位编号
>date,字符串形式，格式：'2016-12-02'


- **返回值**

>以获取血压为例，返回值如下：
>{"code":"0","data":[{"bloodPressure":"0","created":1480608000000},{"bloodPressure":"0","created":1480609800000},{"bloodPressure":"0","created":1480611600000},{"bloodPressure":"0","created":1480613400000},{"bloodPressure":"0","created":1480615200000},{"bloodPressure":"0","created":1480617000000},{"bloodPressure":"0","created":1480618800000},{"bloodPressure":"0","created":1480620600000},{"bloodPressure":"0","created":1480622400000},{"bloodPressure":"0","created":1480624200000},{"bloodPressure":"0","created":1480626000000},{"bloodPressure":"0","created":1480627800000},{"bloodPressure":"0","created":1480629600000},{"bloodPressure":"0","created":1480631400000},{"bloodPressure":"0","created":1480633200000},{"bloodPressure":"0","created":1480635000000},{"bloodPressure":"0","created":1480636800000},{"bloodPressure":"0","created":1480638600000},{"bloodPressure":"0","created":1480640400000},{"bloodPressure":"0","created":1480642200000},{"bloodPressure":"0","created":1480644000000},{"bloodPressure":"0","created":1480645800000},{"bloodPressure":"0","created":1480647600000},{"bloodPressure":"0","created":1480649400000},{"bloodPressure":"0","created":1480651200000},{"bloodPressure":"0","created":1480653000000},{"bloodPressure":"0","created":1480654800000},{"bloodPressure":"0","created":1480656600000},{"bloodPressure":"0","created":1480658400000},{"bloodPressure":"0","created":1480660200000},{"bloodPressure":"0","created":1480662000000},{"bloodPressure":"0","created":1480663800000},{"bloodPressure":"0","created":1480665600000},{"bloodPressure":"0","created":1480667400000},{"bloodPressure":"0","created":1480669200000},{"bloodPressure":"0","created":1480671000000},{"bloodPressure":"0","created":1480672800000},{"bloodPressure":"0","created":1480674600000},{"bloodPressure":"0","created":1480676400000},{"bloodPressure":"0","created":1480678200000},{"bloodPressure":"0","created":1480680000000},{"bloodPressure":"0","created":1480681800000},{"bloodPressure":"0","created":1480683600000},{"bloodPressure":"0","created":1480685400000},{"bloodPressure":"0","created":1480687200000},{"bloodPressure":"0","created":1480689000000},{"bloodPressure":"0","created":1480690800000},{"bloodPressure":"0","created":1480692600000}]}

- **其它**

>返回值说明
>bloodPressure，血压
>heartRate，心率
>temperature,体温



##13.直播开始
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/Helmet/Live/start

- **参数**

>liveId,直播id
>status,状态(0开始直播)

- **返回值**

>{"code":"0","message":"操作成功"}

- **其它**



##14.结束直播
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/Helmet/Live/end

- **参数**

>liveId,直播id
>status,状态(1结束直播)

- **返回值**

>{"code":"0","message":"操作成功"}

- **其它**



##15.获取指定工位提定日期的点播视频数据
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/workervideo

- **参数**

>workerId,工位id
>date,日期
>page,页数，从1开始
>pageSize,每页条数

- **返回值**

>{
     "code": "0",
     "data": [
         {
             "created": 1480690127000,
             "duration": "60",
             "id": 4,
             "liveDate": "2016-12-02",
             "liveId": 5,
             "remark": "",
             "screenshot": "http://114.215.201.40:8080/SmartHelmet/uploads/video_screenshot/20161202224847u=56592966,912324676&fm=11&gp=0.jpg",
             "videoUrl": "http://www.baidu.com/test.mp3",
             "workerId": 13
         },
         {
             "created": 1480690013000,
             "duration": "120",
             "id": 3,
             "liveDate": "2016-12-02",
             "liveId": 5,
             "remark": "备注备注",
             "screenshot": "http://114.215.201.40:8080/SmartHelmet/uploads/avaters/20161202224653u=755038089,3914265036&fm=23&gp=0.jpg",
             "videoUrl": "http://www.baidu.com/test.mp3",
             "workerId": 13
         }
     ]
 }

- **其它**

>返回值说明
duration>,时长
>liveDate，直播日期
>screenshot，视频截图
>videoUrl,播放地址
>workerId，工位id

>http，post方式提交参数


##16.发送找回密码验证码
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/sendmail

- **参数**

>email

- **返回值**

>{"code":"0","data":"e0d345d9-ac95-4867-91c8-e555182c50fe","message":"邮件发送成功"}
>注：返回的data节点将用于verifyEmailCode接口中作为token传递

>{"code":"1","message":"没有找到该用户"}

- **其它**



##17.验证并设置新密码
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/verifyEmailCode

- **参数**

>email
>verifyCode，邮件中的验证码
>newPassword,新密码
>confirmPassword，确认密码

>token,通过http header传递，该参数是通过sendmail接口收到的data节点数据

- **返回值**

>{"code":"0","message":"新密码设置成功"}
>{"code":"2","message":"验证码不正确"}

- **其它**



##18.采集方位信息
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/Helmet/collectPosition

- **参数**

>helmetCode,头盔序列号
>x
>y
>z

- **返回值**

>{"code":"0","message":"采集成功"}
>{"code":"1","message":"缺少必要参数"}

- **其它**

>http post方式提交


##19.根据工位获取其配戴的头盔信息
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/helmetInfo

- **参数**

>workerId,工位编号

- **返回值**

>{"code":"0","data":{"code":"h10029","created":1481184720000,"id":1,"residualElectricity":"10","workingTime":"112"}}

- **其它**

>返回值说明
>residualElectricity,剩余电量
>workingTime,工作时长


##20.获取指定工位指定日期的方位信息
- **调用地址**

>http://114.215.201.40:8080/SmartHelmet/app/api/positions

- **参数**

>date,日期
>workerId,工位编号

- **返回值**

>{"code":"0","data":{"positions":[{"created":1481166682000,"helmetId":13,"id":1,"workerId":10,"x":"1","y":"2","z":"3"},{"created":1481166978000,"helmetId":13,"id":2,"workerId":10,"x":"1","y":"2","z":"3"},{"created":1481167079000,"helmetId":13,"id":3,"workerId":10,"x":"1","y":"2","z":"3"}],"sceneLength":500,"sceneWidth":500}}

- **其它**

>返回值说明
>sceneLength,场景长
>sceneWidth,场景宽



##3.****************
- **调用地址**

>http://114.215.201.40:8080/app/api/************

- **参数**

- **返回值**

- **其它**



##3.****************
- **调用地址**

>http://114.215.201.40:8080/app/api/************

- **参数**

- **返回值**

- **其它**
